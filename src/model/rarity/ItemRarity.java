package model.rarity;

/**
 *
 * Enumeration used to associate the rarity.
 *
 */
public enum ItemRarity {
    /**
    * Type of rarity.
    *
    * VERY COMMON.
    */
    VERY_COMMON,
    /**
     * COMMON.
     */
    COMMON,
    /**
     * RARE.
     */
    RARE,
    /**
     * VERY RARE.
     */
    VERY_RARE;

}
