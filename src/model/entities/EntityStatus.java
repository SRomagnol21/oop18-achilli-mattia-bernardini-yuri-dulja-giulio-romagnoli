package model.entities;

/**
 * 
 * This enum represent the current entity's status, it can be ALIVE, DEAD or FLOWN_AWAY(if is duck).
 *
 */

public enum EntityStatus {
    /**
     * Entity status.
     */
    DEAD, ALIVE, FLOWN_AWAY;
}
