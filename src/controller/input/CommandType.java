package controller.input;
/**
 * 
 * @author giuli
 *
 */
public enum CommandType {

    /**
     * Every possible command.
     */
    PAUSE, RECHARGE, SHOOT;

}
